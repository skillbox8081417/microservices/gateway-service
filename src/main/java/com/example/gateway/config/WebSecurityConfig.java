package com.example.gateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Slf4j
@Configuration
@EnableWebFluxSecurity
public class WebSecurityConfig {

    @Bean
    SecurityWebFilterChain configure(ServerHttpSecurity http) {
        // @formatter:off
        http.csrf()
                .disable()
                .oauth2Client()
                .and()
            .authorizeExchange()
            .pathMatchers("/actuator/**", "/eureka/**", "/login/**","/logout/**", "/oauth2/**", "/user-service/**", "/auth/**")
                .permitAll()
            .anyExchange() //.permitAll();
                .authenticated()
                .and()
                .oauth2Login()
                .and()
            .logout();
        return http.build();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }
//    @Bean
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
}
